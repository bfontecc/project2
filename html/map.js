/**
 * map.js
 *
 * javascript to interact with the embedded google map
 * uses AJAX calls to php back-end
 *
 */

var poly = "";
var map = "";
var theInfoWindow = new google.maps.InfoWindow();

/**
 * initialize
 * 
 */
function init() {
	//authors Alain Ibrahim and Google
	var mapOptions = {
          center: new google.maps.LatLng(37.775362, -122.417564),
          zoom: 12,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
            map = new google.maps.Map(document.getElementById("map_canvas"),
            mapOptions);
    //end of Alain's code
}


/**
 * route()
 * route will be called by the dropdown menu
 * @param routeId
 */
 function route(routeId){
	$.ajax({
  		type: "GET",
  		url: "index.php",
  		data: { q: "route", route_id: routeId },
  		success: function(response) {
  			var points = new Array();	//points will be used to make a polyline.
			var routeColor = response.color;
			var routeConfig = response.config;
			//for each station abbr in config
			for (i = 0; i < routeConfig.length; i++) {
				var abbr = routeConfig[i];
				var lat = response[abbr].station_lat;
				var lng = response[abbr].station_lng;
				//use generate latlng to get coords
				coords = new google.maps.LatLng(lat, lng);
				//add coords to points
				points.push(coords);
				addMarker(coords, abbr);
			}
			//make polyline, passing points
			poly = new google.maps.Polyline({
            	path: points,
            	strokeColor: response.color,
            	strokeOpacity: 1.0,
            	strokeWeight: 2,
            	map: map
        	}); 
  		}
	});
}
 

/**
 * addMarker()
 *
 * @param coords
 * @param abbr
 *
 * creates a marker, adds a listener which loads departure data
 *
 */
function addMarker(coords, abbr) {
	m = new google.maps.Marker({
		position: coords, 
		title: abbr,
		map: map
	});
	//m add listener
	google.maps.event.addListener(m, 'click', function() {
		$.ajax({
  		type: "GET",
  		url: "index.php",
  		data: { q: "departure", s: abbr },
  		success: function(response) {
				var infowindow = new google.maps.InfoWindow({
				position: coords,
    			content: response,
    			map: map
				});
			}
		});
	});
}

