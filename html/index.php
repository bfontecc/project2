<?
/**
 * index.php
 *
 * default dispatcher/controller
 */
$title = "Bret's BARTs Google Maps Mashups";	//the user is encouraged to say it 3 times fast

if (empty($_GET)) {
	include_once("../view/header.php");
	include_once("../view/map.php");
} else {
	if (!isset($_GET['q']))
		break;
	switch ($_GET['q']) {
		default:
			break;
		case 'route':
			include_once("../model/route_model.php");
			include_once("../controller/route_controller.php");
			break;
		case 'departure':
			include_once("../controller/departure_controller.php");
			break;
	}
}