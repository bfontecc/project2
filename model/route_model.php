<?php
/**
 * route_model.php
 *
 * interacts with mySql db to retrieve information for route controller
 * returns information in JSON format
 */

require_once("db_func.php");

//$dbh = get_handle();
//print_r(json_encode(get_route($dbh, 1), JSON_PRETTY_PRINT));		//dbg, development

/**
 * get_json_route()
 *
 * wrapper function which json encodes data returned by get_route()
 *
 * @param route_id
 * 
 * @return json formatted string route
 */
function get_json_route($route_id) {
	$dbh = get_handle();
	return json_encode(get_route($dbh, $route_id));
}
/**
 * get_route()
 *
 * return JSON formatted info for the route
 * stations should be in order
 *
 * @param route_id (passed from controller)
 * @param dbh
 *
 * @return json encoded string route
 */
function get_route($dbh, $route_id) {
	$route = array();
	$route['color'] = get_route_color($dbh, $route_id);
	$route['name'] = get_route_name($dbh, $route_id);
	$route['config'] = get_route_config($dbh, $route_id);
	foreach ($route['config'] as $abbr) {
		$route[$abbr] = get_station($dbh, $abbr);
	}
	return $route;
}

/**
 * get_route_config()
 *
 * @return numeric array of station abbreviations, in order
 *
 * @param $route_id
 * @param $dbh
 * returns empty array on failure
 */
function get_route_config($dbh, $route_id) {
	$query = "SELECT station_abbr FROM configs 
				WHERE route_id=:route_id_bind;";
	$query = $dbh->prepare($query);
	$query->bindValue(':route_id_bind', $route_id, PDO::PARAM_INT);
	$query->execute();
	$selection = $query->fetchAll();
	//print_r($selection);
	$config = array();
	foreach ($selection as $station) {
		if(isset($station[0]))
			array_push($config, $station[0]);
	}
	//print_r($config);
	return $config;
}

/**
 * get_route_color()
 *
 * @param dbh
 * @param route_id
 *
 * @return string route_color
 * returns error string on failure
 */
function get_route_color($dbh, $route_id) {
	$query = "SELECT route_color FROM routes 
				WHERE route_id=:route_id_bind;";
	$query = $dbh->prepare($query);
	$query->bindValue(':route_id_bind', $route_id, PDO::PARAM_INT);
	$query->execute();
	$selection = $query->fetchAll();
	//print_r($selection);
	if (isset($selection[0][0])):
		return $selection[0][0];
	else:
		return "error finding route color";
	endif;
}


/**
 * get_route_name()
 *
 * @param dbh
 * @param route_id
 *
 * @return string route_name
 * returns error string on failure
 */
function get_route_name($dbh, $route_id) {
	$query = "SELECT route_name FROM routes 
				WHERE route_id=:route_id_bind;";
	$query = $dbh->prepare($query);
	$query->bindValue(':route_id_bind', $route_id, PDO::PARAM_INT);
	$query->execute();
	$selection = $query->fetchAll();
	if (isset($selection[0][0])):
		return $selection[0][0];
	else:
		return "error finding route name";
	endif;
}

 
/**
 * get_station()
 *
 * @param string station_abbr
 * @return array with keys 'station_name', 'station_lat', 'station_lng'
 * returns false on failure
 */
function get_station($dbh, $station_abbr) {
	$query = "SELECT station_name, station_lat, station_lng FROM stations 
				WHERE station_abbr=:station_abbr_bind;";
	$query = $dbh->prepare($query);
	$query->bindValue(':station_abbr_bind', $station_abbr, PDO::PARAM_INT);
	$query->execute();
	$selection = $query->fetchAll();
	//print_r($selection);
	if (!isset($selection[0]['station_name']) || 
		!isset($selection[0]['station_lat']) ||
		!isset($selection[0]['station_lng'])):
		return false;
	else:
		return array('station_name'=> $selection[0]['station_name'],
					'station_lat'=> $selection[0]['station_lat'],
					'station_lng'=> $selection[0]['station_lng']
		);
	endif;
}

?>