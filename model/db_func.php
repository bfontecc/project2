<?
/**
 * db_func.php
 *
 * functions to query the mySql database
 * expects constants DB_HOST, DB_DATABASE, DB_USER, DB_PASSWORD to be defined.
 *
 * WARNING: pdo quote instead of bind is not necessarily safe.
 * some input is not cleansed. this should never be accessed by anyone untrusted.
 * permissions should be 700 for this file.
 */

define('DB_HOST', 'localhost');
define('DB_USER', 'jharvard');
define('DB_PASSWORD', 'crimson');
define('DB_DATABASE', 'jharvard_project2');


 /**
 * get_handle()
 * connect to mysql server, select database, and return handle
 * 
 * @return PDO Object $dbh
 */
 function get_handle() {
 	$dbh = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_DATABASE, DB_USER, DB_PASSWORD);
 	return $dbh;
 }
 
 
/**
 * do_insert()
 *
 * @param PDO object dbh
 * @param string table
 * @param numeric array columns
 * @param numeric array values
 *
 * @return bool success
 */
function do_insert($dbh, $table, $columns, $values) {
	//make sure columns and values are matching in length
	$ccount = count($columns);
	$vcount = count($values);
	if ($ccount != $vcount):
		return false;
	endif;
	//begin crafting query
	$query_upper = "INSERT INTO {$table} (";
	$query_lower = "VALUES (";
	//fill in column names
	for ($i = 0; $i < $ccount; $i++) {
		$query_upper .= $columns[$i];
		if ($i < ($ccount-1)):
			$query_upper .= ", ";
		else:
			$query_upper .= ") ";
		endif;
	}
	echo $query_upper;		//dbg
	//fill in values
	for ($j = 0; $j < $vcount; $j++) {
		if (is_numeric($values[$j])):
			$query_lower .= $values[$j];
		else:
			$query_lower .= $dbh->quote($values[$j]);
		endif;
		if ($j < ($vcount-1)):
			$query_lower .= ", ";
		else:
			$query_lower .= ");";
		endif;
	}
	echo $query_lower . "\n";		//dbg
	//prepare query
	$query = $dbh->prepare($query_upper.$query_lower);
	//execute query and return true or false
	return $query->execute();
}

?>
