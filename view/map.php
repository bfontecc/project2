<?php 
/**
 * map.php
 *
 * load google api
 * load bootstrap jquery and dropdown
 * load nav_bar
 * load and initialize map.js
 * 
 */
 ?>
    <body>
    
    <?php include_once("../view/nav_bar.php"); ?>
    
	<!-- author Alain Ibrahim. author Google. -->
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
      html { height: 100% }
      body { height: 100%; margin: 0; padding: 0 }
      #map_canvas { height: 100% }
      
      #select_route {width:20%;height:200px;}
    </style>
    <script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?sensor=false">
    </script>
    <!-- end Alain's code -->
    
    <!-- map.js -->
    <script type="text/javascript" src="map.js"></script>
    <!-- jquery.js -->
    <script type="text/javascript" src="/bootstrap/docs/assets/js/jquery.js"></script>
    <!-- bootstrap-dropdown.js -->
    <script type="text/javascript" 
    	src="/bootstrap/docs/assets/js/bootstrap-dropdown.js"></script>
    
<div id="map_canvas" style="width:100%; height:100%"></div>

<!-- initialize -->
<script type="text/javascript">
$(document).ready(function() {
	init();
});
</script>
