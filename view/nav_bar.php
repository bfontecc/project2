<!-- adapted from example in Bootstrap github-->
<ul class="nav nav-pills">
    <li class="dropdown" id="drop">
    	<a class="dropdown-toggle" data-toggle="dropdown" href="#">
    	Routes
    <b class="caret"></b>
    </a>
    	<ul class="dropdown-menu">
    		<li><a href="#" onclick=route(1)>Bay Point - Millbrae</a></li>
    		<li><a href="#" onclick=route(2)>Millbrae - Bay Point</a></li>
    		<li class="divider"></li>
    		<li><a href="#" onclick=route(3)>Fremont - Richmond</a></li>
    		<li><a href="#" onclick=route(4)>Richmond - Fremont</a></li>
    		<li class="divider"></li>
    		<li><a href="#" onclick=route(5)>Fremont - Daly City</a></li>
    		<li><a href="#" onclick=route(6)>Daly City - Fremont</a></li>
    		<li class="divider"></li>
    		<li><a href="#" onclick=route(7)>Richmond - Daly City</a></li>
    		<li><a href="#" onclick=route(8)>Daly City - Richmond</a></li>
    		<li class="divider"></li>
    		<li><a href="#" onclick=route(11)>Dublin - Daly City</a></li>
    		<li><a href="#" onclick=route(12)>Daly City - Dublin</a></li>
   	 	</ul>
    </li>
</ul>