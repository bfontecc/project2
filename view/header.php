<?php
/**
 * header.php
 * generate header
 */
?>

<!DOCTYPE html>
<html>
    <head>
        <title> 
            <?php echo htmlspecialchars($title) ?>
        </title>

    <!--Twitter Bootstrap Library-->
    <!--license: https://github.com/twitter/bootstrap/wiki/License-->
    <link rel="stylesheet" href="/bootstrap/docs/assets/css/bootstrap.css">
    	<!-- style sheet that might come in handy -->
        <style type="text/css">

		</style>
    </head>