File Structure for Project2:

/bootstrap
	/docs/assets/js/jquery.js
	/docs/assets/js/bootstrap-dropdown.js
	/docs/assets/css/bootstrap.css
/html
	index.php				default controller/dispatcher
/model
	db_func.php				functions for querying the database 
	route_model.php			uses PDO to talk to mySql DB
/view
	nav_bar.php				includes drop-down menus for selecting route
	map.php					the mashup
	header.php				title and bootstrap css
/controller
	departure_controller.php requests departure data from BART's API, which is the model
	route_controller.php	requests route info from route_model
route_cache_creator.php		fills db with data from bart regarding stations and routes

	I decided not to minify javascript due to readability for TA. I also decided to include
the header, footer, and body in one document, due to the unique nature of this single page. I
realize this is normally detrimental to extensibility, but I found it to be the clearest way
to organize this webapp, and I expect that attempting to reuse headers here could depend on
what is being added to the app.
	Unfortunately, departure data was temporarily unavailable during the final stages of debugging,
and it is not clear to me whether route_controller has a bug in it, or this is temporary.