<?php
/**
 * departure_controller
 *
 * checks for station abbreviation s in get request.
 *
 * Communicates directly with BART's Real API.
 * Determines departure information based on etd (estimated time of departure).
 * ex. API query:
 * http://api.bart.gov/api/etd.aspx?cmd=etd&key=MIHZ-QGU3-IDPI-VRE8 &orig=
 * try FRMT for an orig value
 *
 * api key:
 *		MIHZ-QGU3-IDPI-VRE8
 *
 * offers html to requesting party via print.
 */
 
//set header
header("Content-type: text/plain");

define('API_KEY', 'MIHZ-QGU3-IDPI-VRE8');

//check get
if(isset($_GET['s'])){
	$station_abbr = htmlspecialchars($_GET['s']);
	$content = get_departure($station_abbr);
	print($content);
}


get_departure('FRMT');

/**
 * get_departure()
 *
 * @param string station_abbr
 *
 * @return string content
 */
function get_departure($station_abbr) {
	$content = "<!--content from departure model-->" . PHP_EOL;
	//define these html elements for use in the loop
	$start_list = "<ul>";
	$end_list = PHP_EOL . "</ul>";
	$item = PHP_EOL . "<li>";
	$end_item = "</li>";
	//access xml
	$bart = simplexml_load_file("http://api.bart.gov/api/etd.aspx?cmd=etd&key=" 
								. API_KEY . "&orig={$station_abbr}");
	if ($bart === false)
		return;
	//get numeric array of etd XMLStatement objects
	$etd_array = $bart->xpath('station/etd');
	//print_r($etd_array);				//dbg
	//start pretty-printing
	$content .= "Next Trains:<br/>" . PHP_EOL;
	foreach ($etd_array as $etd) {
		//print_r($etd);				//dbg
		$estimate_array = $etd->xpath('estimate');
		//print_r($estimate_array); 	//dbg
		$dest_label = "<b>Destination: " . $etd->xpath('destination')[0] . "</b><br/>" . PHP_EOL;
		$content .= $dest_label;
		$i = 1;
		foreach ($estimate_array as $estimate) {
			$num_label = PHP_EOL . "Departure " . $i . ":" . PHP_EOL;
			$time_label = "Time until departure (min): "
						.  $estimate->xpath('minutes')[0];
			$line_label = "Line: " . $estimate->xpath('color')[0];
			$content .= $num_label . $start_list . $item . $time_label
					. $end_item . $item . $line_label . $end_item . $end_list;
			$i++;
		}
	}
	return $content;
}



