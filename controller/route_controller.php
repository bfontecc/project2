<?php
/**
 * route_controller
 *
 * check get values for route_id
 * cleanse route_id (although route_id will go through bind
 *
 * call get_json_route() in model (model is included by dispatcher index.php)
 * print the result
 *
 * should be accessed by index.php?q=route&route_id=
 */
 
//set header
header("Content-type: application/json");

//check get
if(isset($_GET['route_id'])){
	$route_id = htmlspecialchars($_GET['route_id']);
	$out = get_json_route($route_id);
	print($out);
}

?>