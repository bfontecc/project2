<?php
/**
 * route_cache_creator()
 *
 * request info regarding routes and stations from BART's Real API
 * use it to fill in the tables in the database.
 */

define('API_KEY', 'MIHZ-QGU3-IDPI-VRE8');
require_once("db_func.php");
$dbh = get_handle();

/** uncomment a line (or more) and run the script
  * the database table should be empty, i.e.
  * deleted line by line or dropped and recreated
  */
//fill_stations($dbh);
//fill_routes($dbh);

/**
 * fill_stations()
 * 
 * use stns command
 * add to stations table
 * columns station_name, station_abbr, station_lat, station_lng
 *
 * @param dbh
 *
 * http://api.bart.gov/api/stn.aspx?cmd=stns&key=
 */
function fill_stations($dbh) {
	$columns = ['station_name', 'station_abbr', 'station_lat', 'station_lng'];
	$url = "http://api.bart.gov/api/stn.aspx?cmd=stns&key=" .API_KEY;
	//retrieve xml
	$bart = simplexml_load_file($url);
	if ($bart === false):
		echo "error loading stns command xml";
		return;
	endif;
	//get numeric array of station XMLStatement objects
	$station_array = $bart->xpath('stations/station');
 	foreach($station_array as $station){
 		$values = array();
 		//load the values array
 		array_push($values, (string)$station->name);
 		array_push($values, (string)$station->abbr);
 		array_push($values, (string)$station->gtfs_latitude);
 		array_push($values, (string)$station->gtfs_longitude);
 		do_insert($dbh, "stations", $columns, $values);
 	}
 }
 
/**
 * fill_routes()
 *
 * use routes command
 * add to routes table
 * columns route_id, route_name, route_color
 *
 * @param dbh
 *
 * call fill_config for each route_id, after inserting that route
 *
 * http://api.bart.gov/api/route.aspx?cmd=routes&key=
 */
function fill_routes($dbh) {
	$columns = ['route_id', 'route_name', 'route_color'];
	$url = "http://api.bart.gov/api/route.aspx?cmd=routes&key=" .API_KEY;
	$bart = simplexml_load_file($url);
	if ($bart === false):
		echo "error loading routes command xml";
		return;
	endif;
	//get routes/route array of objects
	$route_array = $bart->xpath('routes/route');
	print_r($route_array);
	foreach($route_array as $route) {
		$route_name = (string) $route->name;
		$route_id = (int) $route->number;
		$route_color = (string) $route->color;
		$values = [$route_id, $route_name, $route_color];
		//do_insert($dbh, "routes", $columns, $values);
		fill_config($dbh, $route_id);
	}
	
}
 
/**
 * fill_config()
 *
 * @param dbh
 * @param route_id
 *
 * use routeinfo command
 * add to configs table
 * columns route_id, pos, route_color
 *
 * must be called in conjuction with fill_routes() due to foreign key constraints
 *
 * http://api.bart.gov/api/route.aspx?cmd=routeinfo&route=6&key=
 */
 function fill_config($dbh, $route_id) {
	$columns = ['route_id', 'pos', 'station_abbr'];
	$url = "http://api.bart.gov/api/route.aspx?cmd=routeinfo&route={$route_id}&key=" .API_KEY;
	//retrieve xml
	$bart = simplexml_load_file($url);
	if ($bart === false):
		echo "error loading routeinfo command xml";
		return;
	endif;
	//get numeric array where item 0 is an xmlelements obj containing station property
	$temp = $bart->xpath('routes/route/config');
	//get station property
	$station_array = $temp[0]->station;
	$pos = 0;
	foreach($station_array as $station) {
		$station_abbr = (string) $station;
		$values = [$route_id, $pos, $station_abbr];
		do_insert($dbh, "configs", $columns, $values);
		$pos++;
	}
	
}